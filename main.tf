
#----------------------------------------------------------------
# CONFIGURE OUR GCP CONNECTION
#----------------------------------------------------------------

provider "google-beta" {
  version = "~> 2.7.0"
  project = var.project
  region  = var.region
}

terraform {
  # The modules used in this example have been updated with 0.12 syntax, which means the example is no longer
  # compatible with any versions below 0.12.
  required_version = ">= 0.12"

# backend "gcs" {
#   bucket = "tf-cloud-sql-bucket-state"
# }

}
#----------------------------------------------------------------
#  configure_kubectl
#----------------------------------------------------------------
# configure kubectl with the credentials of the GKE cluster
resource "null_resource" "configure_kubectl" {
  provisioner "local-exec" {
    command = "gcloud beta container clusters get-credentials ${var.cluster_name} --region ${var.location} --project ${var.project}"

    # Use environment variables to allow custom kubectl config paths
    environment = {
      KUBECONFIG = var.kubectl_config_path != "" ? var.kubectl_config_path : ""
    }
  }

}

#----------------------------------------------------------------
#  load kubernetes provider
#----------------------------------------------------------------
provider "kubernetes" {
  config_context_cluster  = var.config_context_cluster != "" ? var.config_context_cluster : "gke_${var.project}_${var.location}_${var.cluster_name}"
}


#----------------------------------------------------------------
#  load service account module
#----------------------------------------------------------------
module "gke_service_account" {
  # When using these modules in your own templates, you will need to use a Git URL with a ref attribute that pins you
  # to a specific version of the modules, such as the following prod:
   source = "git::https://gitlab.com/myJourneyIaC/GCPInfra/svc-acct.git"

  name        = var.gke_addons_service_account_name
  project     = var.project
  description = var.gke_addons_service_account_description

  service_account_roles = [ "roles/dns.admin","roles/owner",
  ]

}

#----------------------------------------------------------------
#  create a time_sleep resource
#----------------------------------------------------------------
resource "time_sleep" "wait_20_seconds" {
  create_duration = "20s"
}

#----------------------------------------------------------------
#  create a radom resource
#----------------------------------------------------------------
resource "random_string" "suffix" {
  length  = 4
  special = false
  upper   = false
}

#----------------------------------------------------------------
#  create a private key for the service_account
#----------------------------------------------------------------
data "google_service_account" "service_account" {
  account_id = module.gke_service_account.name
  project    = var.project
}

resource "google_service_account_key" "service_account_key" {
  service_account_id = data.google_service_account.service_account.name
}
