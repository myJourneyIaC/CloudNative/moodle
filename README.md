![](images/moodle.jpg)

# Lecture:
* [Bitnami Postgresql](https://github.com/helm/charts/tree/master/stable/postgresql)
* [Bitname Moodle](https://github.com/bitnami/charts/tree/master/bitnami/moodle/#installing-the-chart)
* [Bitname Helm Moodle](https://bitnami.com/stack/moodle/helm)
