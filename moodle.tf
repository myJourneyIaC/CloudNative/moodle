
#----------------------------------------------------------------
#  MOODLE
#----------------------------------------------------------------

resource "helm_release" "moodle" {

  name          = "moodle-psql"
  chart         = "moodle"
  repository    = "https://charts.bitnami.com/bitnami"
  namespace     = var.namespace 
  replace       = true

  values = [
    yamlencode(
      { 
         image = { 
                   repository = "bitnami/postgresql"
         }

         externalDatabase = { 
                   host = var.db_host
                   username = var.master_user_name
                   password = var.master_user_password
                   database = var.db_name

         }
  })
  ]

}
